'use strict'

import AccessService from '../services/access.service.js'
import { CREATED, SuccessResponse } from '../core/success.response.js'

class AccessController {
  handlerRefreshToken = async (req, res, next) => {
    new SuccessResponse({
      message: `Get token success!`,
      metadata: await AccessService.handleRefreshToken({
        keyStore: req.keyStore,
        user: req.user,
        refreshToken: req.refreshToken,
      }),
    }).send(res)
  }

  logout = async (req, res, next) => {
    new SuccessResponse({
      message: `Logout success!`,
      metadata: await AccessService.logout(req.keyStore),
    }).send(res)
  }

  login = async (req, res, next) => {
    new SuccessResponse({
      metadata: await AccessService.login(req.body),
    }).send(res)
  }

  signUp = async (req, res) => {
    console.log(`[P]::signUp::`, req.body)

    new CREATED({
      message: 'Registered OK!',
      metadata: await AccessService.signUp(req.body),
    }).send(res)
  }
}

let controller = new AccessController()

export default controller
