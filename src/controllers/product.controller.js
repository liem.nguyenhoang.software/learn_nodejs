'use strict'

import ProductService from '../services/product.service.js'
import { SuccessResponse } from '../core/success.response.js'

class ProductController {
  createProduct = async (req, res, next) => {
    new SuccessResponse({
      message: 'Create new Product success!',
      metadata: await ProductService.createProduct(req.body.product_type, {
        ...req.body,
        product_shop: req.user.userId,
      }),
    }).send(res)
  }
}

let controller = new ProductController()

export default controller
