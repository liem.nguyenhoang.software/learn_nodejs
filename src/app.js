'use strict'

import express from 'express'
import morgan from 'morgan'
import helmet from 'helmet'
import compression from 'compression'
import routers from './routes/index.js'
import instanceMongodb from './dbs/init.mongodb.js'

const app = express()

// init middlewares
app.use(morgan('combined'))
app.use(helmet())
app.use(compression())
app.use(express.json())
app.use(
  express.urlencoded({
    extended: true,
  }),
)

// init db
instanceMongodb

// init routes
app.use('/', routers)

// handling errors
app.use((req, res, next) => {
  const error = new Error('Not Found')
  error.status = 404
  next(error)
})
app.use((error, req, res, next) => {
  const statusCode = error.status || 500
  return res.status(statusCode).json({
    status: 'error',
    code: statusCode,
    stack: error.stack,
    message: error.message || 'Internal Error',
  })
})

export default app
