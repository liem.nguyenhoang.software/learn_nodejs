'use strict'

import shopModel from '../models/shop.model.js'
import bcrypt from 'bcrypt'
import crypto from 'crypto'
import KeyTokenService from './keyToken.service.js'
import { findByEmail } from './shop.service.js'
import { createTokenPair } from '../auth/authUtils.js'
import { getInfoData } from '../utils/index.js'
import {
  AuthFailureError,
  BadRequestError,
  ForbiddenError,
} from '../core/error.response.js'

const RoleShop = {
  SHOP: '001',
  WRITER: '002',
  EDITOR: '003',
  ADMIN: '004',
}
class AccessService {
  static handleRefreshToken = async ({ keyStore, user, refreshToken }) => {
    const { userId, email } = user
    if (keyStore.refreshTokensUsed.includes(refreshToken)) {
      await KeyTokenService.deleteKeyById(userId)
      throw new ForbiddenError(`Something wrong happened! Please re login!`)
    }

    if (keyStore.refreshToken !== refreshToken)
      throw new AuthFailureError(`Shop not registered!`)

    const foundShop = await findByEmail({ email })
    if (!foundShop) throw new AuthFailureError(`Shop not registered`)

    const tokens = await createTokenPair(
      { userId, email },
      keyStore.publicKey,
      keyStore.privateKey,
    )

    await keyStore.updateOne({
      $set: { refreshToken: tokens.refreshToken },
      $addToSet: {
        refreshTokensUsed: refreshToken,
      },
    })

    return {
      user,
      tokens,
    }
  }

  static logout = async keyStore => {
    const delKey = await KeyTokenService.removeKeyById(keyStore._id)
    return delKey
  }

  static login = async ({ email, password, refreshToken = null }) => {
    // check email in databases
    const foundShop = await findByEmail({ email })
    if (!foundShop) throw new BadRequestError(`Shop not registered!`)

    // check match password
    const match = bcrypt.compare(password, foundShop.password)
    if (!match) throw new AuthFailureError(`Authentication Error`)

    // created privateKey, publicKey
    const privateKey = crypto.randomBytes(64).toString('hex')
    const publicKey = crypto.randomBytes(64).toString('hex')

    // generate tokens
    const { _id: userId } = foundShop
    const tokens = await createTokenPair(
      { userId, email },
      publicKey,
      privateKey,
    )

    // add refreshTokensUsed
    await KeyTokenService.createKeyToken({
      userId,
      refreshToken: tokens.refreshToken,
      privateKey,
      publicKey,
    })

    return {
      shop: getInfoData({
        fields: ['_id', 'name', 'email'],
        object: foundShop,
      }),
      tokens,
    }
  }

  static signUp = async ({ name, email, password }) => {
    // step1: check email exists ?
    const holderShop = await shopModel.findOne({ email }).lean()
    if (holderShop) throw new BadRequestError(`Error: Shop already registered!`)

    const passwordHash = await bcrypt.hash(password, 10)
    console.log('object 1')
    const newShop = await shopModel.create({
      name,
      email,
      password: passwordHash,
      roles: [RoleShop.SHOP],
    })
    if (newShop) {
      // created privateKey, publicKey
      const privateKey = crypto.randomBytes(64).toString('hex')
      const publicKey = crypto.randomBytes(64).toString('hex')

      // save collection KeyStore
      console.log({ privateKey, publicKey })

      const keyStore = await KeyTokenService.createKeyToken({
        userId: newShop._id,
        publicKey,
        privateKey,
      })

      if (!keyStore) {
        throw new BadRequestError(`Error: keyStore error`)
      }

      // create token pair
      const tokens = await createTokenPair(
        { userId: newShop._id, email },
        publicKey,
        privateKey,
      )
      console.log(`Created Token Success::`, tokens)

      return {
        code: 201,
        metadata: {
          shop: getInfoData({
            fields: ['_id', 'name', 'email'],
            object: newShop,
          }),
          tokens,
        },
      }
    }
    return {
      code: 200,
      metadata: null,
    }
  }
}

export default AccessService
