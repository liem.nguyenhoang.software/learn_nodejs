'use strict'

import ProductModel from '../models/product.model.js'
import { BadRequestError } from '../core/error.response.js'

class ProductFactory {
  static productRegistry = {}

  static registerProductType(type, classRef) {
    ProductFactory.productRegistry[type] = classRef
  }

  static async createProduct(type, payload) {
    const productClass = ProductFactory.productRegistry[type]
    if (!productClass)
      throw new BadRequestError(`Invalid Product Types ${type}`)

    return new productClass(payload).createProduct()
  }
}

class Product {
  constructor({
    product_name,
    product_thumb,
    product_description,
    product_price,
    product_quantity,
    product_type,
    product_shop,
    product_attributes,
  }) {
    this.product_name = product_name
    this.product_thumb = product_thumb
    this.product_description = product_description
    this.product_price = product_price
    this.product_quantity = product_quantity
    this.product_type = product_type
    this.product_shop = product_shop
    this.product_attributes = product_attributes
  }

  async createProduct(product_id) {
    return await ProductModel.product.create({ ...this, _id: product_id })
  }
}

class Clothing extends Product {
  async createProduct() {
    const newClothing = await ProductModel.clothing.create(
      this.product_attributes,
    )
    if (!newClothing) throw new BadRequestError(`Create new Clothing error`)

    const newProduct = await super.createProduct()
    if (!newProduct) throw new BadRequestError(`Create new Product error`)

    return newProduct
  }
}

class Electronics extends Product {
  async createProduct() {
    const newElectronic = await ProductModel.electronic.create({
      ...this.product_attributes,
      product_shop: this.product_shop,
    })
    if (!newElectronic) throw new BadRequestError(`Create new Clothing error`)

    const newProduct = await super.createProduct(newElectronic._id)
    if (!newProduct) throw new BadRequestError(`Create new Product error`)

    return newProduct
  }
}

class Furniture extends Product {
  async createProduct() {
    const newFurniture = await ProductModel.furniture.create({
      ...this.product_attributes,
      product_shop: this.product_shop,
    })
    if (!newFurniture) throw new BadRequestError(`Create new Furniture error`)

    const newProduct = await super.createProduct(newFurniture._id)
    if (!newProduct) throw new BadRequestError(`Create new Furniture error`)

    return newProduct
  }
}

// register product types
ProductFactory.registerProductType(`Electronics`, Electronics)
ProductFactory.registerProductType(`Clothing`, Clothing)
ProductFactory.registerProductType(`Furniture`, Furniture)

export default ProductFactory
