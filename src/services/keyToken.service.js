'use strict'

import keyTokenModel from '../models/keyToken.model.js'
import { Types } from 'mongoose'

class KeyTokenService {
  static createKeyToken = async ({
    userId,
    publicKey,
    privateKey,
    refreshToken,
  }) => {
    try {
      const filter = { user: userId }
      const update = {
        publicKey,
        privateKey,
        refreshTokensUsed: [],
        refreshToken,
      }
      const options = { upsert: true, new: true }

      const tokens = await keyTokenModel.findOneAndUpdate(
        filter,
        update,
        options,
      )

      return tokens?.publicKey
    } catch (error) {
      return error
    }
  }

  static findByUserId = async userId => {
    return await keyTokenModel.findOne({ user: new Types.ObjectId(userId) })
  }

  static removeKeyById = async id => {
    return await keyTokenModel.deleteOne(id)
  }

  static findByRefreshTokensUsed = async refreshToken => {
    return await keyTokenModel
      .findOne({ refreshTokensUsed: refreshToken })
      .lean()
  }

  static findByRefreshToken = async refreshToken => {
    return await keyTokenModel.findOne({ refreshToken })
  }

  static deleteKeyById = async userId => {
    return await keyTokenModel.deleteOne({ user: userId }).lean()
  }
}

export default KeyTokenService
