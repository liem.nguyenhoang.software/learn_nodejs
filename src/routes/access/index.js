'use strict'

import express from 'express'
import accessController from '../../controllers/access.controller.js'
import { asyncHandler } from '../../helpers/asyncHandler.js'
import { authentication } from '../../auth/authUtils.js'

const router = express.Router()

router.post('/shop/signup', asyncHandler(accessController.signUp))
router.post('/shop/login', asyncHandler(accessController.login))

router.use(authentication)

router.post('/shop/logout', asyncHandler(accessController.logout))
router.post(
  '/shop/handlerRefreshToken',
  asyncHandler(accessController.handlerRefreshToken),
)

export default router
