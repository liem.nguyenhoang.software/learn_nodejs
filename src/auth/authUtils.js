'use strict'

import JWT from 'jsonwebtoken'
import { asyncHandler } from '../helpers/asyncHandler.js'

// service
import KeyTokenService from '../services/keyToken.service.js'
import { AuthFailureError, NotFoundError } from '../core/error.response.js'

const HEADER = {
  API_KEY: 'x-api-key',
  CLIENT_ID: 'x-client-id',
  AUTHORIZATION: 'authorization',
  REFRESHTOKEN: 'x-rtoken-id',
}

const createTokenPair = async (payload, publicKey, privateKey) => {
  try {
    // accessToken
    const accessToken = await JWT.sign(payload, publicKey, {
      expiresIn: '2 days',
    })

    const refreshToken = await JWT.sign(payload, privateKey, {
      expiresIn: '7 days',
    })

    JWT.verify(accessToken, publicKey, (err, decode) => {
      if (err) console.error(`error verify::`, err)
      else {
        console.log(`decode verify::`, decode)
      }
    })

    return { accessToken, refreshToken }
  } catch (error) {
    return error
  }
}

const authentication = asyncHandler(async (req, res, next) => {
  // Check userId
  const userId = req.headers[HEADER.CLIENT_ID]
  if (!userId) throw new AuthFailureError(`Invalid Request!`)

  // check keyStore with this userId
  const keyStore = await KeyTokenService.findByUserId(userId)
  if (!keyStore) throw new NotFoundError(`Not found keyStore`)

  if (req.headers[HEADER.REFRESHTOKEN]) {
    const refreshToken = req.headers[HEADER.REFRESHTOKEN]
    const decodeUser = JWT.verify(refreshToken, keyStore.privateKey)

    if (userId !== decodeUser.userId)
      throw new AuthFailureError(`Invalid UserId`)
    req.keyStore = keyStore
    req.user = decodeUser
    req.refreshToken = refreshToken

    return next()
  }

  // verify token
  const accessToken = req.headers[HEADER.AUTHORIZATION]
  if (!accessToken) throw new AuthFailureError(`Invalid REquest`)

  const decodeUser = JWT.verify(accessToken, keyStore.publicKey)
  if (userId !== decodeUser.userId) throw new AuthFailureError(`Invalid UserID`)

  req.keyStore = keyStore
  req.user = decodeUser
  return next()
})

const verifyJWT = async (token, keySecret) => {
  return await JWT.verify(token, keySecret)
}

export { createTokenPair, authentication, verifyJWT }
